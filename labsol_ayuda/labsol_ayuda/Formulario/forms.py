from django import forms
from django.db import models
from .models import ContestaFormularios, Campo, Formularios, OpcionesCampo
from django.contrib.auth.decorators import login_required,permission_required

class AplicaFormulario(forms.Form):  
    def __init__(self, *args, **kwargs):
        campos = kwargs.pop('campos')
        
        super(AplicaFormulario, self).__init__(*args, **kwargs)
        
        TIPO_DATO = {
            '1': forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class':'form-control'})),
            '2': forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'})),
            '3': forms.DecimalField(widget=forms.NumberInput(attrs={'class':'form-control'})),
            '4': forms.DateField(widget=forms.DateInput(attrs={'type':'date'})),
            '5': forms.DateTimeField(widget=forms.widgets.DateTimeInput(attrs={'type':'date'})),
            '6': forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control'})),
            '7': forms.ModelChoiceField(queryset=None,widget=forms.Select(attrs={'class':'form-control'})),
            '8': forms.ModelChoiceField(queryset=None, widget=forms.RadioSelect()),
            '9': forms.FileField()
        }
        mi_lista = []

        for campo in campos:
            print(campo.campo)
            if campo.tipo_dato in ['7', '8']:
                mi_lista.append(campo.opciones_campo.all())
            #todo bien
        
        recorrido=0
        for campo in campos:
            self.fields[f"{str(campo.campo)}"] = TIPO_DATO[campo.tipo_dato]
            if campo.tipo_dato in '9':
                self.fields[f"{str(campo.campo)}"] = TIPO_DATO[campo.tipo_dato]
            elif campo.tipo_dato in ['7', '8']:
                self.fields[f"{str(campo.campo)}"].queryset = mi_lista[0]
                print(mi_lista[recorrido])
                recorrido=recorrido+1
                print("lo hizo")
            elif campo.requerida == '0':
                self.fields[f"{str(campo.campo)}"].required = False
            elif campo.tipo_dato == '1':
                self.fields[f"{str(campo.campo)}"].max_length = campo.size_texto
            #todo bien
            
            

class FormCampos(forms.ModelForm):
    class Meta:
        model = Campo
        fields = '__all__'

class FormCampos2(forms.ModelForm):
    class Meta:
        model = Campo
        exclude = ['formularios']

class FormFormularios(forms.ModelForm):
    class Meta:
        model = Formularios
        fields = '__all__'

class FormFiltroSolicitud(forms.Form):  
    formularios = forms.CharField( 
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Modalidad'}),
        required=False
    )   
    usuario = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Usuario'}),
        required=False
    )

class FormFiltroCampos(forms.Form):  
    requerida = forms.CharField( 
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'requerida'}),
        required=False
    )   
    tipo_dato = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'tipo_dato'}),
        required=False
    )


class FormOpcionesCampo1(forms.ModelForm):
    class Meta:
        model = OpcionesCampo
        exclude = ['campo']

def handle_uploaded_file(f):
	with open('Documentos/Usuario/'+f, 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)

class FormCampos2(forms.ModelForm):
    class Meta:
        model = Campo
        exclude = ['formularios']