from django.urls import path
from Formulario import views
from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('ModalidadesLista', login_required(views.ListaFormulario.as_view()), name='modalidades_lista'),

    path('Nueva', login_required(views.CrearFormularioView.as_view()), name='nuevoFormulario'),

    path('Editar/<int:pk>', login_required(views.EditarFormularioView.as_view()), name='editarFormulario'),

    path('eliminar/<int:id>', views.eliminar_formulario, name='eliminar_Formulario'),

    path('responder/<int:id>', views.formulario, name='formulario'),

    path('campos/<int:id>', views.modalidadesCamposLista, name='campos_Lista'),

    path('eliminar/campo/<int:id>', views.eliminar_campo, name='eliminar_campo'),

    path('editar/campo/<int:id>', views.editar_campo, name='editar_campo'),

    path('Nuevo/campo/<int:id>', views.nuevo_campo1, name='nuevo_campo'),

    path('solicitudes', views.lista_solicitudes, name='solicitudes_lista'),

    path('Mis_solicitudes', views.lista_solicitudes1, name='solicitudes_lista1'),

    path('opcion_campo/Nuevo/<int:id>', views.opcionCampoNuevo, name='opcion_campo_Nuevo'),

    path('Detalles/modalidad_solicitud/<int:pk>', login_required(views.ListaSolicitudes.as_view()), name='lista_solicitudes'),
]
