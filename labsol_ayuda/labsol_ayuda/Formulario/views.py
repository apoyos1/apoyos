from django.shortcuts import render, redirect
from .models import Formularios, ContestaFormularios, DetalleContestaFormularios, Campo, Formularios, OpcionesCampo
from .forms import AplicaFormulario, FormCampos2, FormCampos, FormFiltroCampos, FormFormularios, FormOpcionesCampo1, FormFiltroSolicitud
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView
from django.contrib.auth.decorators import login_required,permission_required

@login_required
def formulario(request, id):
    nom_formulario=""
    formulario=Formularios.objects.filter(id=id)
    for formu in formulario:
        nom_formulario=formu.nombre
    quiz = Formularios.objects.get(id=id)
    form = AplicaFormulario(request.FILES,campos=quiz.campos.all())
    if request.method == 'POST':
        form = AplicaFormulario(request.POST,request.FILES,campos=quiz.campos.all()) 
        if form.is_valid():
            data = form.cleaned_data
            contesta_formulario = ContestaFormularios.objects.create(
                formularios = quiz,
                usuario = request.user
            )
            
            for campo in quiz.campos.all():
                respuesta="null"
                if campo.tipo_dato == '9':
                    DetalleContestaFormularios.objects.create(
                        campo = campo,
                        respuesta = data[f"{campo.campo}"],
                        uploadedFile = data[f"{campo.campo}"],
                        contesta_formularios = contesta_formulario
                    )
                else:
                    DetalleContestaFormularios.objects.create(
                        campo = campo,
                        respuesta = data[f"{campo.campo}"],
                        contesta_formularios = contesta_formulario
                    )
    context = {
        # 'quiz': quiz,
        'form': form,
        'nom_formulario':nom_formulario
    }
    # Load documents for the list page
    return render(request, 'formulario.html', context)

@login_required
def nuevo_campo1(request, id):
    if request.method == 'POST':
        form = FormCampos2(request.POST)
        if form.is_valid():
            form = Campo.objects.create(
                campo = request.POST.get('campo', None),
                requerida = request.POST.get('requerida', None),
                tipo_dato = request.POST.get('tipo_dato', None),
                size_texto = request.POST.get('size_texto', None),
                formularios_id = id
            )
            return redirect('modalidades_lista')
    else:
        print("Aqui tambien")
        form = FormCampos2()
    return render(request, 'campo_form.html', {'form':form})
    
class CrearFormularioView(CreateView):
    model = Formularios
    form_class = FormFormularios
    success_url = reverse_lazy('modalidades_lista')
    extra_context = {'accion':'Nuevo Formulario'}

class EditarFormularioView(UpdateView):
    model = Formularios
    form_class = FormFormularios
    extra_context = {'accion':'Modificar'}
    success_url = reverse_lazy('modalidades_lista')
    success_message = "Modalidad editada"

@login_required
def eliminar_formulario(request, id):
    Formularios.objects.get(id=id).delete()
    return redirect('modalidades_lista')
    
class ListaFormulario(ListView):
    model = Formularios

@login_required
def eliminar_campo(request, id):
    Campo.objects.get(id=id).delete()
    return redirect('modalidades_lista')

#esto es nuevo
@login_required
def editar_campo(request, id):
    campo = Campo.objects.get(id=id)
    if request.method == 'POST':
        form = FormCampos2(request.POST, instance=campo)
        if form.is_valid():
            form.save()
            return redirect('modalidades_lista')
    else:
        form = FormCampos2(instance=campo)
    return render(request, 'campo_form.html', {'form':form})

@login_required
def modalidadesCamposLista(request, id):
    nom_formulario=""
    form = FormFiltroCampos()
    camposFormularios = Campo.objects.filter(formularios_id=id)
    formulario=Formularios.objects.filter(id=id)
    for formu in formulario:
        nom_formulario=formu.nombre

    if request.method == 'POST':
        form = FormFiltroCampos(request.POST)
        requerida = request.POST.get('requerida', None)
        tipo_dato = request.POST.get('tipo_dato', None)
        if requerida:
            camposFormularios = camposFormularios.filter(requerida__contains=requerida)
        if tipo_dato:
            tipo_dato = camposFormularios.filter(tipo_dato__contains=tipo_dato)
        
    context={
        'camposFormularios': camposFormularios,
        'form':form,
        'id_form':id,
        'nom_formulario':nom_formulario
    }
    return render(request, 'camposFormularios.html', context)

@login_required
def opcionCampoNuevo(request, id):
    if request.method == 'POST':
        form = FormOpcionesCampo1(request.POST)
        if form.is_valid():
            form = OpcionesCampo.objects.create(
                opcion = request.POST.get('opcion', None),
                valor = request.POST.get('valor', None),
                campo_id = id
            )
            return redirect('modalidades_lista')
    else:
        form = FormOpcionesCampo1()
    return render(request, 'campo_form.html', {'form':form})

class ListaSolicitudes(ListView):
    model = DetalleContestaFormularios
    success_url = reverse_lazy('modalidades_lista')
    def get_queryset(self):
        return  DetalleContestaFormularios.objects.filter(contesta_formularios=self.request.resolver_match.kwargs['pk'])

@login_required
def lista_solicitudes(request):
    form = FormFiltroSolicitud()
    contestaFormularios = ContestaFormularios.objects.all()

    if request.method == 'POST':
        form = FormFiltroSolicitud(request.POST)
        formularios = request.POST.get('formularios_id', None)
        usuario = request.POST.get('usuario_id', None)
        if formularios:
            contestaFormularios = contestaFormularios.filter(formularios__contains=formularios)
        if usuario:
            usuario = contestaFormularios.filter(usuario__contains=usuario)
        
    context={
        'contestaFormularios': contestaFormularios,
        'form':form
    }
    return render(request, 'contestaFormularios.html', context)


    #contestaFormularios = ContestaFormularios.objects.get('usuario_id'==id_usuario)

@login_required
def lista_solicitudes1(request):
    form = FormFiltroSolicitud()

    contestaFormularios = ContestaFormularios.objects.filter(usuario=request.user)

    if request.method == 'POST':
        form = FormFiltroSolicitud(request.POST)
        formularios = request.POST.get('formularios_id', None)
        usuario = request.POST.get('usuario_id', None)
        if formularios:
            contestaFormularios = contestaFormularios.filter(formularios__contains=formularios)
        if usuario:
            usuario = contestaFormularios.filter(usuario__contains=usuario)
        
    context={
        'contestaFormularios': contestaFormularios,
        'form':form
    }
    return render(request, 'contestaFormularios.html', context)