from dataclasses import fields
from django import forms
from convocatoria.models import Convocatoria

class DateTimeInput(forms.DateTimeInput):
    input_type = 'date'

class FormConvocatoria(forms.ModelForm):
    class Meta:
        model = Convocatoria
        #fields = '__all__'
        exclude = ['activacion']
        widgets = {
            'fecha_inicio': DateTimeInput(),
            'fecha_cierre': DateTimeInput(),
        }