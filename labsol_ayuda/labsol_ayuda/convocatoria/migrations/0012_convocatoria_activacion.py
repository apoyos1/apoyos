# Generated by Django 4.0.2 on 2023-02-16 06:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('convocatoria', '0011_alter_convocatoria_fecha_cierre_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='convocatoria',
            name='activacion',
            field=models.CharField(choices=[('1', 'Activado'), ('0', 'Desactivado')], default=1, max_length=1),
        ),
    ]
