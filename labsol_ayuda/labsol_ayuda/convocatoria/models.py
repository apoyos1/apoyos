from django.db import models

ACTIVACION=[
    ('1','Activado'),
    ('0','Desactivado'),
]

class Convocatoria(models.Model): 
    fecha_inicio = models.DateTimeField("Fecha_inicio",  null=False)
    fecha_cierre = models.DateTimeField("Fecha_cierre", null=False)
    activacion = models.CharField(choices=ACTIVACION, max_length=1 , default=1)

    def __str__(self):
        return self.fecha_inicio +" "+self.fecha_cierre


