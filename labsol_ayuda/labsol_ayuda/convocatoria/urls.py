from django.urls import path
from convocatoria import views
from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('convocatoria/', login_required(views.ListaConvocatoria.as_view()), name='convocatoria_lista'),
    path('convocatoria/nueva', views.nuevo_convocatoria, name='nueva_convocatoria'),
    #path('convocatoria/nueva',  login_required(views.NuevaCategoriaView.as_view()), name='nueva_convocatoria'),
    path('convocatoria/eliminar/<int:id>', views.eliminar_convocatoria, name='eliminar_convocatoria'),
    path('convocatoria/editar/<int:id>', views.editar_convocatoria, name='editar_convocatoria'),
]